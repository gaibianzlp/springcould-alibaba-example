package com.zlp.service;

import com.zlp.dto.UploadResp;

import java.io.FileNotFoundException;
import java.util.List;

public interface FileService {

    UploadResp unionUpload(List<String> pathUrls) throws FileNotFoundException;

    UploadResp jsonUpload();
}

package com.zlp.config.nacos;

import com.alibaba.fastjson.JSON;
import com.zlp.dto.Upload;
import com.zlp.dto.UploadResp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
/**
 * http://127.0.0.1:8110/testUploadRespJson
 * @date: 2022/3/15 15:38
 * @return:
 */
@Slf4j
@RestController
public class NacosTestController {

    @Resource
    private NacosConfigLocalCatch nacosConfigLocalCatch;

    @GetMapping("/testUploadRespJson")
    public String testUploadRespJson() {

        log.info("===>testUploadRespJson");
        Upload uploadResp = nacosConfigLocalCatch.get("uploadRespJson", Upload.class);
        log.info("testUploadRespJson.resp uploadResp={}",JSON.toJSONString(uploadResp));
        return JSON.toJSONString(uploadResp);
    }

}

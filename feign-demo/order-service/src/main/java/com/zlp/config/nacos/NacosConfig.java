package com.zlp.config.nacos;

import com.zlp.dto.Upload;
import com.zlp.dto.UploadResp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class NacosConfig {

    /**
     * <p>
     *     前提条件 要求  serverAddr、namespace、group使用bootstrap.yml 中 spring cloud nacos config 的。
     * </p>
     * @return
     */
    @Bean
    public Map<String, Class> nacosConfigLocalCacheInfoMap() {
        // key:dataId, value:对应数据类型
        Map<String, Class> map = new HashMap<>();
        map.put("uploadRespJson", Upload.class);
        return map;
    }
}


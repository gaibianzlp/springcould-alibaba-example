package com.zlp.dto;

import lombok.Data;

@Data
public class SyncUploadReportReq {

    /**
     * 患者ID
     */
    private String personId;

    /**
     * 报告ID
     */
    private String reportNo;


    /**
     * 文件类型
     */
    private Integer category;

    /**
     * 源文件地址
     */
    private String sourceUrl;

    /**
     * 数据
     */
    private String data;



}

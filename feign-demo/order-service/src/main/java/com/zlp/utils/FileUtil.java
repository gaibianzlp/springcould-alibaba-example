package com.zlp.utils;


import com.zlp.feign.FileDownFeign;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
@Slf4j
@Component
@AllArgsConstructor
public class FileUtil {

    private final FileDownFeign fileDownFeign;



    /*
     *//**
     * 多图片压缩zip
     * @param srcFiles 图片名称
     * @param zipFile 文件路径
     *//*
	public static void zipFiles(List<String> srcFiles, File zipFile) {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		// 判断压缩后的文件存在不，不存在则创建
		if (!zipFile.exists()) {
			try {
				zipFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// 创建 FileOutputStream 对象
		FileOutputStream fileOutputStream = null;
		// 创建 ZipOutputStream
		ZipOutputStream zipOutputStream = null;
		// 创建 FileInputStream 对象
		BufferedInputStream bis = null;

		try {
			// 实例化 FileOutputStream 对象
			fileOutputStream = new FileOutputStream(zipFile);
			// 实例化 ZipOutputStream 对象
			zipOutputStream = new ZipOutputStream(fileOutputStream);
			// 创建 ZipEntry 对象
			ZipEntry zipEntry = null;
			// 遍历源文件数组
			for (int i = 0; i < srcFiles.size(); i++) {
				// 将源文件数组中的当前文件读入 FileInputStream 流中
				//fileInputStream = new FileInputStream(srcFiles.get(i));
				OSSObject ossObject = ossClient.getObject(bucketName, srcFiles.get(i));
				bis = new BufferedInputStream(ossObject.getObjectContent());
				String myurl=srcFiles.get(i);
				// 文件后缀名称
				myurl=myurl.substring(myurl.lastIndexOf("."), myurl.length());
				// 实例化 ZipEntry 对象，源文件数组中的当前文件
				zipEntry = new ZipEntry("DMS/picture/"+System.currentTimeMillis()+myurl);
				zipOutputStream.putNextEntry(zipEntry);
				// 该变量记录每次真正读的字节个数
				int len;
				// 定义每次读取的字节数组
				byte[] buffer = new byte[2048];
				while ((len = bis.read(buffer)) > 0) {
					zipOutputStream.write(buffer, 0, len);
				}

			}
			zipOutputStream.closeEntry();
			zipOutputStream.close();
			bis.close();
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 关闭OSSClient。
	    ossClient.shutdown();
	}*/


    /**
     * 多图片压缩zip
     *
     * @param srcFiles 图片名称
     * @param zipFile  文件路径
     */
    public  void zipFeignFiles(List<String> srcFiles, File zipFile) {

        // 判断压缩后的文件存在不，不存在则创建
        if (!zipFile.exists()) {
            try {
                zipFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // 创建 FileOutputStream 对象
        FileOutputStream fileOutputStream ;
        // 创建 ZipOutputStream
        ZipOutputStream zipOutputStream ;
        // 创建 FileInputStream 对象
        BufferedInputStream bis = null;

        FileInputStream inputStream = null;
        // 创建 InputStream 对象
        InputStream is = null;

        try {
            // 实例化 FileOutputStream 对象
            fileOutputStream = new FileOutputStream(zipFile);
            // 实例化 ZipOutputStream 对象
            zipOutputStream = new ZipOutputStream(fileOutputStream);
            // 创建 ZipEntry 对象
            ZipEntry zipEntry ;
            // 遍历源文件数组
            for (String file : srcFiles) {
                // 将源文件数组中的当前文件读入 FileInputStream 流中

                ResponseEntity<byte[]> responseEntity = fileDownFeign.getObject(file);
                // ======= check
                if (Objects.nonNull(responseEntity)) {
                    byte[] body = responseEntity.getBody();
                    log.info("feign===> response.file.seze={}",body.length);
                    if (Objects.nonNull(body)) {
                        is = new ByteArrayInputStream(body);
                    }
                }
                // =======
                String fileName = file;
                // 文件后缀名称
                // 实例化 ZipEntry 对象，源文件数组中的当前文件
                zipEntry = new ZipEntry("测试"+File.separator+fileName);
                zipOutputStream.putNextEntry(zipEntry);
                // 该变量记录每次真正读的字节个数
                int len;
                // 定义每次读取的字节数组
                byte[] buffer = new byte[4096];
                while ((len = is.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, len);
                }
            }
            zipOutputStream.closeEntry();
            zipOutputStream.close();

            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) {
                    bis.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

/*
    *//**
     * 多图片压缩zip
     *
     * @param srcFiles 图片名称
     * @param zipFile  文件路径
     *//*
    public static void zipFiles(List<String> srcFiles, File zipFile) {

        // 判断压缩后的文件存在不，不存在则创建
        if (!zipFile.exists()) {
            try {
                zipFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // 创建 FileOutputStream 对象
        FileOutputStream fileOutputStream ;
        // 创建 ZipOutputStream
        ZipOutputStream zipOutputStream ;
        // 创建 FileInputStream 对象
        BufferedInputStream bis = null;

        FileInputStream inputStream = null;

        try {
            // 实例化 FileOutputStream 对象
            fileOutputStream = new FileOutputStream(zipFile);
            // 实例化 ZipOutputStream 对象
            zipOutputStream = new ZipOutputStream(fileOutputStream);
            // 创建 ZipEntry 对象
            ZipEntry zipEntry ;
            // 遍历源文件数组
            int index = 1;
            for (String file : srcFiles) {
                // 将源文件数组中的当前文件读入 FileInputStream 流中
                String fileName = file;
                String localFileDir = "D:\\file\\video"+"\\图片"+index;
                fileName = URLDecoder.decode(fileName, "UTF-8");
                //获取文件输入流  localFileDir是服务端存储文件的路径
                File files = new File(localFileDir + File.separator + fileName);
                inputStream = new FileInputStream(files);
                // 文件后缀名称
                // 实例化 ZipEntry 对象，源文件数组中的当前文件
                zipEntry = new ZipEntry("\\图片"+index+File.separator+fileName);
                zipOutputStream.putNextEntry(zipEntry);
                // 该变量记录每次真正读的字节个数
                int len;
                // 定义每次读取的字节数组
                byte[] buffer = new byte[4096];
                while ((len = inputStream.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, len);
                }
                index++;

            }
            zipOutputStream.closeEntry();
            zipOutputStream.close();

            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) {
                    bis.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/
    /**
     * 多图片压缩zip
     *
     * @param srcFiles 图片名称
     * @param zipFile  文件路径
     */
    public static void zipFiles(List<String> srcFiles, File zipFile) {

        // 判断压缩后的文件存在不，不存在则创建
        if (!zipFile.exists()) {
            try {
                zipFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // 创建 FileOutputStream 对象
        FileOutputStream fileOutputStream ;
        // 创建 ZipOutputStream
        ZipOutputStream zipOutputStream ;
        // 创建 FileInputStream 对象
        BufferedInputStream bis = null;

        FileInputStream inputStream = null;

        try {
            // 实例化 FileOutputStream 对象
            fileOutputStream = new FileOutputStream(zipFile);
            // 实例化 ZipOutputStream 对象
            zipOutputStream = new ZipOutputStream(fileOutputStream);
            // 创建 ZipEntry 对象
            ZipEntry zipEntry ;
            // 遍历源文件数组
            int index = 1;
            for (String file : srcFiles) {
                // 将源文件数组中的当前文件读入 FileInputStream 流中
                String fileName = file;
                String localFileDir = "D:\\file\\video"+"\\图片"+index;
                fileName = URLDecoder.decode(fileName, "UTF-8");
                //获取文件输入流  localFileDir是服务端存储文件的路径
                File files = new File(localFileDir + File.separator + fileName);
                inputStream = new FileInputStream(files);
                // 文件后缀名称
                // 实例化 ZipEntry 对象，源文件数组中的当前文件
                if (index == 4) {
                    fileName = "测试.txt";
                    fileName = URLDecoder.decode(fileName, "UTF-8");
                    zipEntry = new ZipEntry("\\源数据\\报告\\图片"+index+File.separator+fileName);
                }else{

                    zipEntry = new ZipEntry("\\源数据\\图片"+index+File.separator+fileName);
                }
                zipOutputStream.putNextEntry(zipEntry);
                // 该变量记录每次真正读的字节个数
                int len;
                // 定义每次读取的字节数组
                byte[] buffer = new byte[4096];
                while ((len = inputStream.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, len);
                }
                index++;

            }
            zipOutputStream.closeEntry();
            zipOutputStream.close();

            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) {
                    bis.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取类路径
     * return 绝对路径地址
     */
    public static String getTemplatePath() {
        String realPath = FileUtil.class.getClassLoader().getResource("").getFile();
        File file = new File(realPath);
        realPath = file.getAbsolutePath();
        try {
            realPath = java.net.URLDecoder.decode(realPath, "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return realPath;
    }

    public static void main(String[] args) {
        System.out.println(getTemplatePath());
    }

}

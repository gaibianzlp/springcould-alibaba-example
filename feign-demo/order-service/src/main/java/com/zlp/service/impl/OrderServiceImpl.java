package com.zlp.service.impl;

import com.alibaba.fastjson.JSON;
import com.zlp.dto.UploadResp;
import com.zlp.feign.FileDownFeign;
import com.zlp.feign.FileUploadFeign;
import com.zlp.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Objects;

@Service
@AllArgsConstructor
@Slf4j(topic = "OrderServiceImpl")
public class OrderServiceImpl implements OrderService {

    private final FileDownFeign fileDownFeign;
    private final FileUploadFeign fileUploadFeign;


    @Override
    public void getObject(HttpServletResponse response, String fileUrl) {
        getImgDown(response, fileUrl);



    }

    private void getVideoDown(HttpServletResponse response,String fileUrl) {

        ResponseEntity<byte[]> responseEntity = fileDownFeign.getObject(fileUrl);
        BufferedInputStream bis = null;
        if (Objects.nonNull(responseEntity)) {
            byte[] body = responseEntity.getBody();
            if (Objects.nonNull(body)) {
                try {
                    log.info("文件大小==>:{}", body.length);
                    String filename = "20210113001.mp4";
                    InputStream is = new ByteArrayInputStream(body);
                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-disposition", "attachment; filename=" + new String(filename.getBytes("utf-8"), "ISO8859-1"));
                    bis = new BufferedInputStream(is);
                    OutputStream os = response.getOutputStream();
                    byte[] buffer = new byte[2048];
                    int i = bis.read(buffer);
                    while (i != -1) {
                        os.write(buffer, 0, i);
                        i = bis.read(buffer);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (bis != null) {
                        try {
                            bis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }

    }

    private void getImgDown(HttpServletResponse response,String fileUrl) {

        ResponseEntity<byte[]> responseEntity = fileDownFeign.downloadFile(fileUrl);
        BufferedInputStream bis = null;
        if (Objects.nonNull(responseEntity)) {
            byte[] body = responseEntity.getBody();
            if (Objects.nonNull(body)) {
                try {
                    log.info("文件大小==>:{}", body.length);
                    String filename = "20210113001.jpg";
                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-disposition", "attachment; filename=" + new String(filename.getBytes("utf-8"), "ISO8859-1"));
                    InputStream is = new ByteArrayInputStream(body);
                    bis = new BufferedInputStream(is);
                    OutputStream os = response.getOutputStream();
                    byte[] buffer = new byte[2048];
                    int i = bis.read(buffer);
                    while (i != -1) {
                        os.write(buffer, 0, i);
                        i = bis.read(buffer);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (bis != null) {
                        try {
                            bis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }

    }
    @Override
    public UploadResp uploadFile(MultipartFile file) {

        log.info("uploadFile==>");
        String type = "suirenow";
        UploadResp uploadResp = fileUploadFeign.uploadFile(file, type);
        if (Objects.nonNull(uploadResp)) {
            log.info("fileUploadFeign.uploadFile.resp uploadResp={}", JSON.toJSONString(uploadResp));
        }
        return uploadResp;
    }

}

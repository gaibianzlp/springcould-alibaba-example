package com.zlp.service;

import com.zlp.dto.UploadResp;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface OrderService {




    void getObject(HttpServletResponse response, String fileUrl);

    UploadResp uploadFile(MultipartFile file);
}

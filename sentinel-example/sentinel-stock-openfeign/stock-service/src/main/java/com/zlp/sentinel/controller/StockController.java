package com.zlp.sentinel.controller;

import com.zlp.sentine.stock.feign.StockFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/stock")
public class StockController {

    @Resource
    StockFeignService stockFeignService;

    @GetMapping("/deductStock")
    public String deductStock() throws InterruptedException {
        System.out.println("扣除库存!");
        String s = stockFeignService.reduct(1);
        return "Hello Feign" + s;
    }
}

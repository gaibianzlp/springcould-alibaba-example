package com.zlp.sentine.stock.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="sentinel-stock",contextId = "StockFeignService")
public  interface StockFeignService {

    @RequestMapping("/reduct")
     String reduct(@RequestParam(name = "orderId") int orderId) ;

}

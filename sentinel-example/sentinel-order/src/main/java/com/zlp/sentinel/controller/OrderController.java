package com.zlp.sentinel.controller;


import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.zlp.sentinel.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping(value = "/addOrder")
    public String addOrder(int orderId) {
        return orderService.addOrder(orderId);
    }


    @GetMapping(value = "/sayHello")
    public String sayHello(String name){
        return orderService.sayHello(name);
    }

    @GetMapping("/flow")
    //@SentinelResource(value = "flow",blockHandler = "flowBlockHandler")
    public String flow() throws InterruptedException {

        return "正常访问";
    }

    public String flowBlockHandler(BlockException e){
        return "流控";
    }


    @GetMapping("/flowThread")
    public String flowThread() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        log.info("flowThread======>正常访问");
        return "正常访问";
    }


    // 关联流控   访问/add 触发/get
    @GetMapping("/add")
    public String add(){
        System.out.println("下单成功!");
        return "生成订单";
    }
    // 关联流控  访问/add 触发/get
    @RequestMapping("/get")
    public String get() throws InterruptedException {
        return "查询订单";
    }

    @RequestMapping("/err")
    public String err()  {
        int a =1/0;
        return "hello";
    }



    @GetMapping(value = "/test3")
    public String test3() throws InterruptedException {

        return orderService.getUser();
    }

    @GetMapping(value = "/test4")
    public String test4() throws InterruptedException {

        return orderService.getUser();
    }


}

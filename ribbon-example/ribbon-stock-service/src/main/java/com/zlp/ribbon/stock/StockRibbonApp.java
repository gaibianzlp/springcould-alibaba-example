package com.zlp.ribbon.stock;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockRibbonApp {

    public static void main(String[] args) {
        SpringApplication.run(StockRibbonApp.class,args);
    }


}

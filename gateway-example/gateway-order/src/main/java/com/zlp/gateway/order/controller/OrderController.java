package com.zlp.gateway.order.controller;


import com.zlp.gateway.order.dto.OrderDTO;
import com.zlp.gateway.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping("getOrder")
    public OrderDTO getOrder(){
        return orderService.getOrder();
    }

    @GetMapping("order/getOrderDetail")
    public OrderDTO getOrderDetail(){
        return orderService.getOrder();
    }


}

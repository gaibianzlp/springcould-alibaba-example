package com.zlp.gateway.service.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("getHello")
    public String getHello(){
        return "Gateway Hello World";
    }
}

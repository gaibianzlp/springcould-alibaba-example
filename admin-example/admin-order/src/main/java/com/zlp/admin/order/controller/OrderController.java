package com.zlp.admin.order.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

// https://blog.csdn.net/weixin_45929667/article/details/103661674
@Slf4j
@RestController
public class OrderController {

    @GetMapping("getOrder")
    public String getOrder() {
        log.info("getOrder.execute");
        Date date = new Date();
        // SimpleDateFormat
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(date);
        log.info("getOrder.resp 记录当前时间：普通时间： {}", time);
        return "admin order cliect hello";
    }

    @GetMapping("getError")
    public String getError(int i) {
        log.info("getError.req i={}",i);
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(date);
        int index = 1 / i;
        log.info("getError.resp 记录当前时间：普通时间： {}", time);
        return "admin order cliect hello"+index;
    }


}

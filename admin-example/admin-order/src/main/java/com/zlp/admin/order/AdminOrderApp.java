package com.zlp.admin.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * http://127.0.0.1:8000/getOrder
 * http://localhost:8000/actuator
 * @date: 2021/12/14 9:43
 */
@SpringBootApplication
public class AdminOrderApp {

    public static void main(String[] args) {
        SpringApplication.run(AdminOrderApp.class,args);
    }

}

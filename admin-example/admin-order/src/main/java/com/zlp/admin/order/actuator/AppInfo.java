package com.zlp.admin.order.actuator;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

/**
 * 自定义 info
 * @date: 2021/12/16 18:53
 */
@Component
public class AppInfo implements InfoContributor {


    @Override
    public void contribute(Info.Builder builder) {

        builder.withDetail("version","1.0.RELEASE");
        builder.withDetail("project","admin-order");
        builder.withDetail("desc","admin-order study");

    }
}

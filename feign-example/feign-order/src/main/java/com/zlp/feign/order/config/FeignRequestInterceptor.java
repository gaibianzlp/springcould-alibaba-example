package com.zlp.feign.order.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;


/**
 * Feign 请求头拦截
 * @date: 2021/12/6 21:01
 */
@Configuration
@Slf4j(topic = "FeignRequestInterceptor")
public class FeignRequestInterceptor implements RequestInterceptor {


    @Override
    public void apply(RequestTemplate requestTemplate) {

        String accessToken = UUID.randomUUID().toString();
        log.info("accessToken={}",accessToken);
        requestTemplate.header("Authorization",accessToken);
    }
}

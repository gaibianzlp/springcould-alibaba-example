package com.zlp.feign.order.config;

import feign.Logger;
import feign.Request;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 此处配置@Configuration注解就会全局生效，如果想指定对应微服务生效，就不能配置
 *
 * @date: 2021/12/7 20:03
 * @return:
 */
@Configuration
public class FeignConfig {


    /**
     * 日志级别
     * <p>
     * 1. NONE【性能最佳，适用于生产】：不记录任何日志（默认值）。
     * 2. BASIC【适用于生产环境追踪问题】：仅记录请求方法、URL、响应状态代码以及
     * 执行时间。
     * 3. HEADERS：记录BASIC级别的基础上，记录请求和响应的header。
     * 4. FULL【比较适用于开发及测试环境定位问题】：记录请求和响应的header、body
     * 和元数据。
     * </p>
     *
     * @date: 2021/12/6 20:40
     * @return: feign.Logger.Level
     */
    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.BASIC;
    }

    /**
     *  超时配置
     * @date: 2021/12/7 20:20
     * @return: feign.Request.Options
     */
    @Bean
    public Request.Options options() {
        return new Request.Options(5000, 10000);
    }
}

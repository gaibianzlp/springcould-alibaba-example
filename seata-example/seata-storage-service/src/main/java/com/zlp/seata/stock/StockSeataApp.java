package com.zlp.seata.stock;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockSeataApp {

    public static void main(String[] args) {
        SpringApplication.run(StockSeataApp.class,args);
    }


}

package com.zlp.seata.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.zlp.seata.order.api.StockService;
import com.zlp.seata.order.entity.Order;
import com.zlp.seata.order.mapper.OrderMapper;
import com.zlp.seata.order.service.OrderService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
@Slf4j(topic = "OrderServiceImpl")
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Autowired
    StockService stockService;

    /**
     * 下单
     * @date 2021-12-1 14:51:18
     */
    @Override
    @GlobalTransactional
    public Order create(Order order) {

        log.info("create.req order={}", JSON.toJSONString(order));
        // 插入订单
        orderMapper.insert(order);
        // 扣减库存 能否成功？
        stockService.reduct(order.getProductId());
        // 异常
        int a = 1 / 0;
        return order;
    }


}

package com.zlp.seata.order.entity;


import lombok.Data;

@Data
public class Order {


    private Integer id;

    private Integer productId;

    private Integer totalAmount;

    private Integer status;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", productId=" + productId +
                ", totalAmount=" + totalAmount +
                ", status=" + status +
                '}';
    }
}
package com.zlp.seata.order.service;

import com.zlp.seata.order.entity.Order;


public interface OrderService {

     Order create(Order order);

}

package com.zlp.seata.order.controller;

import com.zlp.seata.order.entity.Order;
import com.zlp.seata.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/order")
public class OrderController {

    private final OrderService orderService;


    @RequestMapping("/add")
    public String add() {
        Order order = new Order();
        order.setProductId(1);
        order.setStatus(0);
        order.setTotalAmount(100);
        orderService.create(order);
        return "下单成功";
    }


}

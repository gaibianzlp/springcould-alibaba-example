package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: LiPing.Zou
 * @create: 2020-05-18 17:02
 **/
@SpringBootApplication
public class ShareApp {

    public static void main(String[] args) {
        SpringApplication.run(ShareApp.class,args);
    }
}

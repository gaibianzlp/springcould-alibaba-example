package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: LiPing.Zou
 * @create: 2020-05-18 17:27
 **/
@SpringBootApplication
public class NameSpaceApp {

    public static void main(String[] args) {
        SpringApplication.run(NameSpaceApp.class,args);
    }
}

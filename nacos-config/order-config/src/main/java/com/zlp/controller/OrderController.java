package com.zlp.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: LiPing.Zou
 * @create: 2020-05-18 15:27
 **/
@RestController
@RefreshScope
public class OrderController {

    @Value("${order.title}")
    private String orderTitle;


    @GetMapping("getOrder")
    public String getOrder(){
        return "order config" +orderTitle;
    }
}

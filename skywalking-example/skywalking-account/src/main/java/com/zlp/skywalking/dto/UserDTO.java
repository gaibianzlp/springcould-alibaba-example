package com.zlp.skywalking.dto;


import lombok.Data;

@Data
public class UserDTO {

    private Long userId;
    private String userName;
    private String address;
    private String traceId;

    private OrderDTO orderDTO;
}

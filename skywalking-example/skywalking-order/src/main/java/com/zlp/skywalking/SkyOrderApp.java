package com.zlp.skywalking;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(value = "com.zlp.skywalking")
public class SkyOrderApp {

    public static void main(String[] args) {
        SpringApplication.run(SkyOrderApp.class,args);
    }


}

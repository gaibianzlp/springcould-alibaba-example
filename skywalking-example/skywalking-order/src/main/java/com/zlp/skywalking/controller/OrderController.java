package com.zlp.skywalking.controller;

import com.zlp.skywalking.dto.OrderDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {


    @GetMapping("/add")
    public OrderDTO add(){
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(1L);
        orderDTO.setOrderNo(System.currentTimeMillis()+"");
        orderDTO.setName("iphone 13pro");
        orderDTO.setPrice(100L);
        return orderDTO;
    }
}

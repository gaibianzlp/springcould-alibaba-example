package com.zlp.skywalking.dto;


import lombok.Data;

@Data
public class OrderDTO {

    private Long id;
    private String orderNo;
    private Long userId;
    private String name;
    private Long price;
}
